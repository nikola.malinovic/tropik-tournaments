export const environment = {
  production: false,
  baseUrl: 'https://tropik-tournaments-staging.herokuapp.com/',
  baseApiUrl: 'https://tropik-tournaments-api-staging.herokuapp.com/'
};
