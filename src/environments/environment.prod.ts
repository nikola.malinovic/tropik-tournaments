export const environment = {
  production: true,
  baseUrl: 'https://tropik-tournaments.herokuapp.com/',
  baseApiUrl: 'https://tropik-tournaments-api.herokuapp.com/'
};
