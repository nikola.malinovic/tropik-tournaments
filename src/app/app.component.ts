import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Test } from './test';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'tropik-tournaments';
  test = 'TEST';

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<Test>(environment.baseApiUrl).subscribe((response => {
      this.test = response.message;
    }));
  }
}
